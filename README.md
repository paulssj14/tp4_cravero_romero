![logo_unsl](imagenes/logo_unsl.png "logo_UNSL")
![logo_dpto](imagenes/logo_dpto.png "logo_dpto")
# **Trabajo Práctico**
## Arquitectura de Computadoras


#### **Integrantes del grupo:**
###### Cravero Federico
###### Romero Coronado Paul Andrés


#### **Profesores:**
###### Ing. Andres Airabella
###### Ing. Astri Andrada


<details><summary> Update 18/07/22: </summary>
Se cambiaron las rutas absolutas usadas en src/ProgMem.vhd en la linea 39 y test_python/ProgMem/ProgMem_tb.py en la linea 13 por rutas relativas.
</details>



# **Trabajo Práctico 4**

## Camino de datos y control del microprocesador

<details><summary> TP4 (Click to expand)</summary>

1. **Genere dibujos independientes para cada uno de los bloques que va a utilizar para
construir el procesador.**

Después de analizar el set de instrucciones de RISC-V se decidió dividirlo en tres grupos con respecto a los bloques que utilizan:

Instrucciones:
1. Aritméticas, lógicas y corrimientos: PC, Memoria de programa, Registros, ALU
2. Load y store: PC, Memoria de programa, Registros, ALU, Memoria de datos
3. Saltos condicionales: PC, Memoria de programa, Registros, ALU, un sumador extra.

- **_Datapath para las instrucciones Aritméticas, lógicas y corrimientos:_**
![datapath_aritmetica](imagenes/datapath_aritmetica.png "datapath_aritmetica")


En la imagen anterior se observa que el PC (program counter) apunta a la dirección de instrucción actual, por cada ciclo de reloj se incrementa en 4 para apuntar a la próxima instrucción. Este le envía una dirección de 64 bits a la memoria de programa que a la salida muestra la instrucción correspondiente. De la instrucción se mandan los respectivos bits a la unidad de control, al banco de registros (direcciones de registros fuente y destino), al bloque de extensión de signo (Imm Gen) y a la ALU Control. Luego, según se utilicen ambos registros fuente o uno de estos y un inmediato será el bit de ALUSrc correspondiente. Por último, la ALU realizará la operación que la ALU Control determine según los bits que recibió y el resultado se guardará en el Banco de Registros (el cual tiene habilitada la escritura por el módulo de Control).

- **_Datapath para las instrucciones Load y Store:_**
![datapath_data_transfer](imagenes/datapath_data_transfer.png "datapath_data_transfer")
En la imagen anterior se puede observar que el datapath es similar al anterior, con el agregado de la memoria de datos. Esta memoria es la que se utiliza para cargar o guardar valores que no pueden mantenerse en el banco de registros. La función de la ALU en este caso es calcular la dirección del valor en la memoria de datos por medio de un registro fuente y un inmediato. Luego, la unidad de control habilita la lectura o escritura según se quiera extraer o guardar un dato en dicha memoria.

- **_Datapath para las instrucciones de salto condicionales:_**
![datapath_condicional](imagenes/datapath_condicional_v3.png "datapath_condicional")
En la imagen anterior se observa que el datapath es similar al primero, con la diferencia de que a este se le ha agregado un sumador y multiplexor en la realimentación del PC (program counter). En este caso la ALU compara valores. Si se cumple la condición se habilita un salto de instrucción con un inmediato.

2. **Realice el dibujo de un Datapath completo para el set de instrucciones propuesto. Indique en el dibujo anchos de todos los buses y nombres de las señales intermedias que luego utilizará en el código.**

Se muestra el datapath final, que es una combinación de los casos anteriores.
![datapath_final](imagenes/datapath_full_v1.png "datapath_final")

3. **Realice una tabla de verdad para todas las señales de control.**

La tabla de verdad se muestra en la siguiente imagen
![tabla_de_verdad](imagenes/Datapath_tabla_de_verdad.png "tabla_de_verdad")

4. **Cree un repositorio en www.gitlab.com siguiendo este tutorial:https://alejandrojs.wordpress.com/2017/06/01/como-empezar-a-usar-git-con-gitlab/**

El repositorio creado se puede acceder a través del siguiente link: [Repositorio](https://gitlab.com/paulssj14/tp4_cravero_romero)

</details>

# **Trabajo Práctico 5**

## Uso de los IP Cores del microprocesador
<details><summary> TP5 (Click to expand)</summary>

1. **Clonar el repositorio con los códigos**

2. **Explorar el contenido del mismo. Identifique en qué directorio se encuentran los IP Cores y en qué directorio los Tests.**
- Los IP Cores se encuentran en el directorio _src_
- Los tests se encuentran en el directorio _python_test_

3. **Ingrese al directorio del test del Banco de Registros y ejecute la simulación. Recordar previamente ejecutar el dockershell.**
![captura_RB](imagenes/captura_RegisterBank_tb.png "captura_RB_tb")

4. **Ubique la ALU. Revise el código de la ALU y el Test correspondiente. A partir de la lista del Set Instrucciones, determine si las operaciones de la ALU cubren todas las instrucciones propuestas en ese set. Si no, realice las modificaciones necesarias en el IP Core de la ALU y en el Test correspondiente, para que todas las instrucciones propuestas puedan ejecutarse**

Las operaciones de la ALU **NO** cubren todas las instrucciones propuestas en ese set. Faltan las operaciones para las instrucciones de salto condicional:
- **BEQ:** Hace un salto cuando dos valores son iguales
- **BNEQ:** Hace un salto cuando dos valores son distintos
- **BLT:** Hace un salto cuando un valor es menor al otro
- **BGE:** Hace un salto cuando un valor es mayor o igual al otro
- **BLTU:** Hace un salto cuando un valor es menor al otro, sin signo
- **BGEU:** Hace un salto cuando un valor es mayor o igual al otro, sin signo

Una vez realizada las modificaciones necesarias en el IP Core de la ALU y en el Test correspondiente, se ejecuta la simulación y se muestra lo que se ve en la siguiente imagen.
![captura_ALU](imagenes/captura_ALU_tb.png "captura_ALU_tb")

</details>

# **Trabajo Práctico 6**

## Armando el procesador

<details><summary> TP6 (Click to expand)</summary>

1. **Genere un nuevo branch en su repositorio a partir de ​master​.**
2. **Diseñe un ​código VHDL de alto nivel ​que incluya todos los IP Cores del procesador y los conecte según el diagrama de bloques que propuso en el TP4​.**
3. **Escriba un programa que utilice todas las instrucciones del set propuesto. Cargar ese programa en la memoria de instrucciones.**

Ese programa se encuentra en el archivo _program.txt_ ubicado en test_python/ProgMem. El contenido de ese archivo representa el siguiente código en assembly

![codigo](imagenes/codigo_tp6.png "codigo")

4. **Realice una simulación donde muestre el estado del contador de programa, salidas de los registros, salida de la ALU, memorias y todas aquellas señales que considere pertinentes para verificar el funcionamiento del procesador.**

La simulación resultó exitosa, se procede a mostrar unas imagenes que lo demuestran
<details><summary>Imagenes tb_tp6 (Click to expand)</summary>
![instruccion_R](imagenes/tb_instruccion_R_add.png "instruccion_R_add")
![instruccion_I](imagenes/tb_instruccion_I_addi.png "instruccion_I_addi")
![instruccion_S](imagenes/tb_instruccion_S_sd.png "instruccion_S_sd")
![instruccion_SB](imagenes/tb_instruccion_SB_beq.png "instruccion_SB_beq")
</details>
![tb_pass_test](imagenes/tb_tp6.png "tb_pass_test")
</details>
