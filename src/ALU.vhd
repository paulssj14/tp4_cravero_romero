library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity alu is

generic (N : integer :=64);
port (
      A_i      : in  std_logic_vector(N - 1 downto 0);
      B_i      : in  std_logic_vector(N - 1 downto 0);
      OUTPUT_o : out std_logic_vector(N - 1 downto 0);
      ALUop_i  : in  std_logic_vector(3 downto 0);
      Zero_o   : out std_logic
      );
end entity;

architecture alu_arch of alu is
--Instrucciones Aritmeticas y logicas
constant AND_IN : std_logic_vector(3 downto 0) := "0000";  -- As defined in the book
constant OR_IN  : std_logic_vector(3 downto 0) := "0001";  -- As defined in the book
constant ADD    : std_logic_vector(3 downto 0) := "0010";  -- As defined in the book
constant XOR_IN : std_logic_vector(3 downto 0) := "0101";
constant SUB    : std_logic_vector(3 downto 0) := "0110";  -- As defined in the book

--Instrucciones de desplazamiento (shift)
constant SLL_IN : std_logic_vector(3 downto 0) := "1000";
constant SRL_IN : std_logic_vector(3 downto 0) := "1001";
constant SLA_IN : std_logic_vector(3 downto 0) := "1010";
constant SRA_IN : std_logic_vector(3 downto 0) := "1011";

--Instrucciones de salto CONDICIONAL
constant BEQ	: std_logic_vector(3 downto 0) := "1100"; -- (Branch if EQual) Salto si A y B son iguales
constant BNEQ 	: std_logic_vector(3 downto 0) := "1110"; -- (Branch if Not EQual) Salto si A y B no son iguales
constant BLT	: std_logic_vector(3 downto 0) := "1111"; -- (Branch if Less than) Salto si A es menor que B, con signo
constant BGE   	: std_logic_vector(3 downto 0) := "1101"; -- (Branch if Greater or equal) Salto si A es mayor igual que B, con signo
constant BLTU   : std_logic_vector(3 downto 0) := "0111"; -- (Branch if Less than Unsigned) Salto si A es menor que B, sin signo
constant BGEU   : std_logic_vector(3 downto 0) := "0011"; -- (BGE Unsigned) Salto si A es mayor igual que B, sin signo

signal zero : std_logic;
signal output_s : std_logic_vector(N-1 downto 0);

begin

 Zero_o <= zero;
 OUTPUT_o <= output_s;

--Zero_o <= '1' when output_s = std_logic_vector(to_unsigned(0,N)) else '0'; 

process(A_i, B_i, ALUop_i)

variable aux1 : std_logic;
variable aux2 : std_logic_vector(N-1 downto 0);

begin
    case ALUop_i is
        when ADD =>
            output_s <= std_logic_vector((unsigned(A_i) + unsigned(B_i)));
            zero <= '0';
        when SUB =>
            output_s <= std_logic_vector((unsigned(A_i) - unsigned(B_i)));
            zero <= '0';
        when AND_IN =>
            output_s <= A_i and B_i;
            zero <= '0';
        when OR_IN =>
            output_s <= A_i or B_i;
            zero <= '0';
        when XOR_IN =>
            output_s <= A_i xor B_i;
            zero <= '0';
        when SLL_IN =>
            output_s <= std_logic_vector(shift_left(unsigned(A_i), to_integer(unsigned(B_i))));
            zero <= '0';
        when SRL_IN =>
            output_s <= std_logic_vector(shift_right(unsigned(A_i), to_integer(unsigned(B_i))));
            zero <= '0';
        when SLA_IN =>
            output_s <= std_logic_vector(shift_left(signed(A_i), to_integer(unsigned(B_i))));
            zero <= '0';
        when SRA_IN =>
            output_s <= std_logic_vector(shift_right(signed(A_i), to_integer(unsigned(B_i))));
            zero <= '0';
       
       --Instrucciones faltantes
        when BEQ =>
            output_s <= (others =>'0');		
            if(A_i = B_i) then
            	zero <= '1';
            	output_s(0) <= '1';
            			    
            else
            	zero <= '0';
            end if;	
        when BNEQ =>
            output_s <= (others =>'0');
            if(A_i /= B_i) then
            	zero <= '1';
            	output_s(0) <= '1'; 
            else
            	zero <= '0';	
            end if;
        when BLT =>
            output_s <= (others =>'0');
            if(A_i < B_i) then
            	zero <= '1';
            	output_s(0) <= '1';
            else
            	zero <= '0';	
            end if;
        when BGE=>
            output_s <= (others =>'0');
            if(A_i >= B_i) then
            	zero <= '1';
            	output_s(0) <= '1'; 
            else 
            	zero <= '0';	
            end if;
        when BLTU =>
            output_s <= (others =>'0');
            if(unsigned(A_i) < unsigned(B_i)) then
            	zero <= '1';
            	output_s(0) <= '1';
            else
            	zero <= '0';	
            end if;
        when BGEU =>
            output_s <= (others =>'0');
            if(unsigned(A_i) >= unsigned(B_i)) then
            	zero <= '1';
            	output_s(0) <= '1';
            else
            	zero <= '0';	
            end if;
        

        when others => output_s <= (others => '0');        
    end case;
    end process;
end alu_arch;
