library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity UC is
    generic(
        opcode  :   integer :=  32
    );
    port(
        INSTR_i         :   in  std_logic_vector(opcode-1 downto 0);            -- instruccion de entrada
        Condbranch_o    :   out std_logic;                                      -- salto
        uncondbranch_o  :   out std_logic;                                      -- salto incondicional
        MemRead_o       :   out std_logic;                                      -- lectura de memoria
        MemtoReg_o      :   out std_logic;                                      -- memoria a registro
        ALUop_o         :   out std_logic_vector(3 downto 0);              		-- seleccion de operacion de ALU
        MemWrite_o      :   out std_logic;                                      -- escritura de memoria
        ALUsrc_o        :   out std_logic;                                      -- selecciona entre un inmediato y un registro
        U_type_o        :   out std_logic;                                      -- permite utilizar lui
        Reg_W_o         :   out std_logic                                       -- escritura de registro                                     
    );
end UC;

architecture control_arch of UC is
    signal opcode_i:    std_logic_vector(6 downto 0);   --opcode de instruccion, hasta 7 bits
    signal func3:       std_logic_vector(2 downto 0);   -- func3 de instruccion, hasta 3 bits 
    signal func7:       std_logic_vector(6 downto 0);   -- func7 de instruccion, hasta 7 bits 
    begin
    opcode_i <= INSTR_i(6 downto 0);      -- se le otorga la parte que corresponde al opcode
    func3 <= INSTR_i(14 downto 12);       -- se le ortorga la parte que corresponde a func3
    func7 <= INSTR_i (31 downto 25);      -- se le otorga la parte qu corresponde a func7   
    Condbranch_o        <= '1' when (opcode_i = "1100111") else '0';
    uncondbranch_o  <= '1' when (opcode_i= "1100011" or opcode_i="1101111") else '0';
    MemRead_o   <= '1' when (opcode_i = "0000011") else '0';
    MemtoReg_o  <= '1' when (opcode_i = "0000011") else '0';
    MemWrite_o  <= '1' when (opcode_i = "0100011") else '0';
    ALUsrc_o        <= '1' when (opcode_i="0000011" or opcode_i = "0010011" or opcode_i= "0100011") else '0'; -- verificar jalr y jal 
    Reg_W_o <= '1' when (opcode_i="0110011" or opcode_i="0000011" or opcode_i= "0010011") else '0';
    
--------------------------------------------------------------------------------
    -- ALUop_o 
    --                  Aritmetico Logicas:

    -- 0000-->0010 ADD (instrucciones= add, addi, ldur, stur)
    -- 0001-->0110 SUB (sub, subs)
    -- 0011-->0000 AND (and, andi)
    -- 0100-->0001 OR  (or, ori)
    -- 0101-->0101 XOR (xor, xori)
    --                  SHIFT:

    -- 0110-->1001 SRL
    -- 0111-->1011 SRA 
    -- 1000 SLL

    --                 LAS FALTANTES:
    -- 1100 BEQ
    -- 1110 BNEQ
    -- 1111 BLT
    -- 1101 BGE
    -- 0111 BLTU
    -- 0011 BGEU
----------------------------------------------------------------------------------
            
    ALUop_o <= "0010" when ((opcode_i = "0110011" and func3="000" and func7="0000000") or (opcode_i= "0000011")  or (opcode_i="0010011" and func3="000") or (opcode_i="0100011") )else --ADD (instrucciones= add, addi, ldur, stur)               
               "0110" when ((opcode_i = "0110011" and func3="000" and func7="0100000")  ) else --SUB(sub, subs)or (opcode_i="1100111")
               "1011" when ((opcode_i = "0110011" and func3="101") or (opcode_i= "0010011" and func3= "101" and func7="0100000"))else--SRA
               "0101" when ((opcode_i = "0110011" and func3="100") or (opcode_i="0010011" and func3="100"))else--XOR(xor, xori)
               "1001" when ((opcode_i = "0110011" and func3="101") or (opcode_i="0010011" and func3="101" and func7="0000000")) else--SRL
               "0001" when ((opcode_i = "0110011" and func3="110") or (opcode_i="0010011" and func3="110")) else--OR(or, ori)
               "1000" when ((opcode_i = "0110011" and func3="001" and func7="0000000") or (opcode_i="0010011" and func3="001" and func7="0000000")) else --SLL
               --FALTA LAS 6 INSTRUCCIONES DE SALTO CONDICIONAL  
               "1100" when (opcode_i = "1100111" and func3="000") else --BEQ
               "1110" when (opcode_i = "1100111" and func3="001") else --BNEQ
               "1111" when (opcode_i = "1100111" and func3="100") else --BLT
               "1101" when (opcode_i = "1100111" and func3="101") else --BGE
               "0111" when (opcode_i = "1100111" and func3="110") else --BLTU
               "0011" when (opcode_i = "1100111" and func3="111") else --BGEU  
               "0000"; --AND    
               
    U_type_o <= '1' when (opcode_i="0110111") else '0';

end control_arch;
