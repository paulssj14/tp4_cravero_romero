import cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, FallingEdge, Edge, Event
from cocotb.result import TestFailure, TestError, ReturnValue, SimFailure

CLK_PERIOD = 10

@cocotb.coroutine
def Reset(dut):
    dut.RST_i <=  0
    yield Timer(CLK_PERIOD * 1)
    dut.RST_i  <= 1
    yield Timer(CLK_PERIOD * 10)
    dut.RST_i  <= 0

@cocotb.test()
def test(dut):
    """
    Description:
        Test RISC-V Simple Implementation
    """
    cocotb.fork(Clock(dut.CLK_i, CLK_PERIOD).start())
    
    yield Reset(dut)
    
    #--Variables auxiliares
    #opcode = " "    
    #--	
    #yield Timer(CLK_PERIOD * 64)
    print("\n\n")
    print("--------------------------------------------------------------------------------------")
    print("INICIO DEL TEST BENCH")
    print("---------------------------------------------------------------------------------------")
    for i in range(31): 
        yield RisingEdge(dut.CLK_i)
        opcode = ""
        tipo = ""
        print("----------------------------------------INICIO DE LA INSTRUCCIÓN-------------------------\n")
        print("---------------------------------------------------------------------------------------\n\n")
        print("PC: ",dut.PCOUT.value.integer)
        print("Memoria de programa:")
        print("\t Instrucción (DATA_o): ", end="")
    #-------Aca empieza la magia, lo que se imprime por pantalla 
        
        #opcode:
        print(dut.Instruction.value[0:6],end=""),

        print("\t",end="")
	
	#rd o inmediato:
        print(dut.Instruction.value[7:11],end=""),
            
        print("\t",end="")

 	#funct3:     
        print(dut.Instruction.value[12:16],end=""),
            
        print("\t",end="")

        #rs1:
        print(dut.Instruction.value[17:19],end=""),
            
        print("\t",end="")
        
 	#rs2 o inmediato:
        print(dut.Instruction.value[20:24],end=""),
            
        print("\t",end="")

	#funct7 o inmediato:
        opcode += str(dut.Instruction.value[25:31])
        print(dut.Instruction.value[25:31],end=""),
        print("\n")
            
        #-------------------------------------------------------------------
        print("Análisis de la Instruccion:")
        if(opcode == "0110011"):
            print("\t Instrucción tipo: R")
            tipo = "R"
        elif(opcode == "0100011"):
            print("\t Instrucción tipo: S")
            tipo = "S"
        elif(opcode == "1100111"):
            print("\t Instrucción tipo: SB")
            tipo = "SB"
        elif(opcode == "0000011" or opcode == "0010011"):
            print("\t Instrucción tipo I:",end=""),
            if(opcode == "0000011"):
            	print(" - Load")
            else:
            	print("\n")
            tipo = "I"
        else:
            print("Formato no válido \n")

        print("\t ALU realiza: ",end=""),
        if(dut.ALUOp.value.integer == 0):
            print(" AND")
        elif(dut.ALUOp.value.integer == 1):
            print(" OR")
        elif(dut.ALUOp.value.integer == 2):
            print(" ADD")
        elif(dut.ALUOp.value.integer == 5):
            print(" XOR")
        elif(dut.ALUOp.value.integer == 6):
            print(" SUB")
        elif(dut.ALUOp.value.integer == 8):
            print(" SLL")
        elif(dut.ALUOp.value.integer == 9):
            print(" SRL")
        elif(dut.ALUOp.value.integer == 10):
            print(" SLA")
        elif(dut.ALUOp.value.integer == 11):
            print(" SRA")
        elif(dut.ALUOp.value.integer == 12):
            print(" BEQ")
        elif(dut.ALUOp.value.integer == 14):
            print(" BNEQ")
        elif(dut.ALUOp.value.integer == 15):
            print(" BLT")
        elif(dut.ALUOp.value.integer == 13):
            print(" BGE")
        elif(dut.ALUOp.value.integer == 7):
            print(" BLTU")
        elif(dut.ALUOp.value.integer == 3):
            print(" BGEU")
        else:
            print(" Funcion desconocida")
            
        print("\t rs1 (A_i): "+"X" + str(dut.Instruction.value[12:16].integer))
        
        if(tipo == "I"):
            if(str(dut.Instruction.value[0:5]) == "010000" or str(dut.Instruction.value[0:5]) == "000000"):
            	#Registro fuente inmediato: 
                print("\t immediate: "+ str(dut.Instruction.value[6:11].integer))
            else:
            	#Registro fuente inmediato: 
                print("\t immediate: "+ str(dut.Instruction.value[0:11].integer))
            
            #Registro destino:
            print("\t rd (C_i): "+"X" + str(dut.Instruction.value[20:24].integer))
        
        elif(tipo == "S"):
            #Registro fuente 2:
            print("\t rs2 (B_i): "+"X" + str(dut.Instruction.value[7:11].integer))
            immediate_int = str(dut.Instruction.value[0:6].integer) + str(dut.Instruction.value[20:24].integer)
            #Inmediato
            print("\t immediate: "+ immediate_int)
        
        elif(tipo == "SB"):
            #Registro fuente 2:
            print("\t rs2 (B_i): "+ "X" + str(dut.Instruction.value[7:11].integer))
            immediate_int = str(dut.Instruction.value[0].integer) + str(dut.Instruction.value[24].integer)  + str(dut.Instruction.value[1:6].integer) + str(dut.Instruction.value[20:23].integer)
            #Inmediato:
            print("\t immediate: "+ immediate_int)
        else: # Tipo R
            #Registro fuente 2:
            print("\t rs2 (B_i): "+"X" + str(dut.Instruction.value[7:11].integer))
            #Registro destino:
            print("\t rd (C_i): "+"X" + str(dut.Instruction.value[20:24].integer))
            
        print("\n")
#-----------------------------------------------------------------        
        print("Banco de registros:")
        print("\t ENTRADAS:")
        #Registro fuente 1;
        print("\t A_i (rs1): "+str(dut.Instruction.value[12:16]))
        
        if(tipo == "I"):
            if(str(dut.Instruction.value[0:5]) == "010000" or str(dut.Instruction.value[0:5]) == "000000"):
          
            #Registro destino:
               print("\t C_i (rd): "+ str(dut.Instruction.value[20:24]))
        
        elif(tipo == "S"):
            #Registro fuente 2:
            print("\t B_i (rs2): "+ str(dut.Instruction.value[7:11]))

        elif(tipo == "SB"):
            #Registro fuente 2:
            print("\t B_i (rs2): "+ str(dut.Instruction.value[7:11]))
        
        else: # Tipo R
            #Registro fuente 2:
            print("\t B_i (rs2): "+ str(dut.Instruction.value[7:11]))
            #Registro destino:
            print("\t C_i (rd): "+ str(dut.Instruction.value[20:24]))
            
        print("\t SALIDAS:")
        print("\t R_a_o:", dut.ReadDATA1toALU.value)
        print("\t R_b_o:", dut.ReadDATA2toALU.value)
        print("\n")
#------------------------------------------------------------------
        print("ALU:")
        print("\t ENTRADAS:")
        print("\t A_i:", dut.ReadDATA1toALU.value)
        print("\t B_i:", dut.MuxtoALU.value)
        print("\t SALIDAS:")
        print("\t Y_o:",dut.ALUresult.value)
        print("\t Zero:",dut.ZERO.value)
        print("\n")
            
        print("Memoria de Datos:")
        print("\t DATA_o (W_c_i):", dut.MUXtoREGISTERS.value)
        print("\n")
        
        print("Unidad de control:") #Orden según la tabla de verdad
        print("\t ALUSrc:",dut.ALUSrc.value)
        print("\t MemtoReg:",dut.MemtoReg.value)
        print("\t RegWrite:",dut.RegWrite.value)        
        print("\t MemRead:",dut.MemRead.value)
        print("\t MemWrite:",dut.MemWrite.value)
        print("\t Branch:",dut.Branch.value)
        print("\t ALUOp:",dut.ALUOp.value)
        print("-----------------------------------------FIN DE LA INSTRUCCIÓN-------------------------\n")
        print("---------------------------------------------------------------------------------------")
        print("\n\n")
