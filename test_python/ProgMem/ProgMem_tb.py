import cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, FallingEdge, Edge, Event
from cocotb.result import TestFailure, TestError, ReturnValue, SimFailure
from cocotb.binary import BinaryValue
import os


CLK_PERIOD = 10
DATA_WIDTH = 32
ADDR_WIDTH = 10

program_file = 'Program.txt'


def build_word(file_handler):
    Value_0 = file_handler.readline()
    Value_1 = file_handler.readline()
    Value_2 = file_handler.readline()
    Value_3 = file_handler.readline()

    value = int(Value_0[0:8] + Value_1[0:8] + Value_2[0:8] + Value_3[0:8], 2)

    return value

def load_memory(file_name, dut):
    pl = program_length(program_file)
    
    if 'incorrect' not in pl:
        dut._log.info('Program Lengh is correct')
        f = open(file_name, 'r')
        instruction_memory = []
        for i in range(int(pl)):
            instruction_memory.append(build_word(f))
    else:
        raise TestFailure(pl)

    return [instruction_memory, int(pl)]


def program_length(file_name):
    f = open(file_name, "r")
    for i, l in enumerate(f):
        pass
    if (i+1)%4 == 0:
        return str(int((i+1)/4))
    else:
        return 'Memory size is incorrect'

@cocotb.coroutine
def InitDUT(dut):
    dut.ADDR_i   <= 0
    dut.DATA_o   <= 0
    dut.CLK_i    <= 0
    dut.RST_i  <= 0

    yield RisingEdge(dut.CLK_i)

    dut.RST_i  <= 1
    
    yield RisingEdge(dut.CLK_i)
    yield RisingEdge(dut.CLK_i)
    yield RisingEdge(dut.CLK_i)

    dut.RST_i  <= 0



@cocotb.test()
def test(dut):
    """
    Description:
        Data memory test
    """
    cocotb.fork(Clock(dut.CLK_i, CLK_PERIOD, units='ns').start())

    yield InitDUT(dut)

    [im, pl] = load_memory(program_file, dut)

    dut._log.info("Program Length %d" % pl)

    yield RisingEdge(dut.CLK_i)

    # i stands for instruction address
    for i in range(0, pl, 1):
        
        
        dut.ADDR_i <= i*4

        yield RisingEdge(dut.CLK_i)

        if i-4 >= 0:
            #print('Address         : 0x%x' % dut.ADDR_i.value.integer)
            #print('Data from Memory: 0x%x' % dut.DATA_o.value.integer)
            #print('Data Expected   : 0x%x' % im[i-1])
            #print('-')
            if dut.DATA_o.value.integer != im[i-1]:
                assert TestFailure("Error! Program Memory is not working properly")
        

    dut._log.info('Program Memory is working fine!')

    yield RisingEdge(dut.CLK_i)
