import cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, FallingEdge, Edge, Event
from cocotb.result import TestFailure, TestError, ReturnValue, SimFailure
from cocotb.binary import BinaryValue
from random import randint


CLK_PERIOD = 10
DATA_WIDTH = 64
ADDR_WIDTH = 10

@cocotb.coroutine
def InitDUT(dut):
    dut.ADDR_i   <= 0
    dut.DATA_i   <= 0
    dut.DATA_o   <= 0
    dut.MemWrite <= 0
    dut.MemRead  <= 0
    dut.CLK_i    <= 0
    yield RisingEdge(dut.CLK_i)



@cocotb.test()
def test(dut):
    """
    Description:
        Data memory test
    """
    cocotb.fork(Clock(dut.CLK_i, CLK_PERIOD).start())

    yield InitDUT(dut)

    dut._log.info('Writing random values in all memory positions.')
    listOfNumbers = [0]
    for x in range (0, 2**ADDR_WIDTH-1):
        listOfNumbers.append(randint(0, 2**64-1))

    dut.MemWrite <= 1

    for i in range(0,2**ADDR_WIDTH-1):
        dut.ADDR_i <= i
        dut.DATA_i <= listOfNumbers[i]
        yield RisingEdge(dut.CLK_i)

    dut.MemWrite <= 0

    yield RisingEdge(dut.CLK_i)
    
    dut.MemRead <= 1

    yield RisingEdge(dut.CLK_i)

    for i in range(0,2**ADDR_WIDTH-1):
        dut.ADDR_i <= i
        yield RisingEdge(dut.CLK_i)
        if(listOfNumbers[i] != dut.DATA_o.value.integer):
            assert TestFailure("Error! Data was not written properly in memory")

    dut.MemRead <= 0
                    
    yield RisingEdge(dut.CLK_i)
